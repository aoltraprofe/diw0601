/*************************************
 Pong
 Pong en js con ayuda de canvas HTML 

 Fecha inicio: 06/02/2017
 Autor: Alfredo Oltra
 Licencia: CC-by-sa 4.0
***************************************/

// enlaza la carga del documento con la función eventWindowLoaded
window.addEventListener("load", eventWindowLoaded, false);

function eventWindowLoaded() {
    canvasPong();
}

// encapsula el canvas del juego
function canvasPong() {

    var canvas = document.getElementById("miCanvas");

    // comprobación del soporte de canvas en el navegador
    if (!canvas.getContext)
        return;

    var context = canvas.getContext("2d");

    // variables juego
    var loopInc = 10,
        stickWidth = 10,
        stickHeight = 80,
        stickMargin = 20,
        ballRadius = 6,
        angle = 45, // ángulo de partida (se modifica en initPoint)
        waitingForLooser = 0;

    // variables partida
    var pointsPlay1 = 0,
        pointsPlay2 = 0,
        posYP1 = (canvas.height - stickHeight) * .5,
        posYP2 = (canvas.height - stickHeight) * .5,
        ball = {
            x: canvas.width * 0.5,
            y: 0
        },
        velPSB = 280, // velocidad por segundo pelota
        speedB,
        velPSP = 500,
        speedP,
        P1isMovig = 0,
        P2isMovig = 0,
        lostP1 = false,
        lostP2 = false;


    // se inicia el juego
    function initGame() {
        pointsPlay1 = 0;
        pointsPlay2 = 0;

        // calculo el número de pixeles que la pelota y los sticks avanzan en loopInc a partir de la velocidad en pixel por segundo
        speedB = velPSB / (1000 / loopInc);
        speedP = velPSP / (1000 / loopInc);

        // enlazo los eventos de pulsación y libración de teclas para gestionar el movimiento de los jugadores
        window.addEventListener("keydown", keyDownHandler, false);
        window.addEventListener("keyup", keyUpHandler, false);

        initPoint();
    }

    // inicio del punto
    function initPoint() {
        // la posición y el ángulo de salida de la pelota cambia en cada punto
        ball.x = canvas.width * .5;
        ball.y = Math.floor((Math.random() * canvas.height * 0.5) + 20); // entre 20 y la mitad de alto
        angle = Math.floor((Math.random() * 80));
    }

    // redibujado de la pantalla
    function drawScreen() {
        context.fillStyle = "#000000";
        context.fillRect(0, 0, canvas.width, canvas.height);

        // dibujo la red
        var anchoRed = 6,
            altoRed = 10,
            sepRed = 1.5; // 50% del alto del tramo como separacion

        context.fillStyle = "#fff";
        var tramosRed = canvas.height / (sepRed * altoRed);
        for (var i = 0; i < tramosRed; i++) {
            context.fillRect((canvas.width - anchoRed) * 0.5, i * (sepRed * altoRed), anchoRed, altoRed);
        }

        // marcador
        context.font = "30px 'Press Start 2P'";

        context.fillText(pointsPlay1, canvas.width * .25 - 15, 50);
        context.fillText(pointsPlay2, canvas.width * .75 - 15, 50);

        drawStick(1, posYP1);
        drawStick(2, posYP2);

        // efecto gol
        if (lostP1 == true || lostP2 == true) {
            context.fillStyle = "#ffaa55";

            if (lostP1 == true)
                context.fillRect(0, 0, 10, canvas.height);
            else
                context.fillRect(canvas.width - 10, 0, canvas.width - 10, canvas.height);

            waitingForLooser++;
            if (waitingForLooser > 50) {
                waitingForLooser = 0;
                initPoint();
                lostP1 = false;
                lostP2 = false;
            }
            return;
        }

        drawBall();
    }

    // dibuja los stick de los jugadores
    function drawStick(player, posY) {
        if (player == 1)
            posX = stickMargin;
        else
            posX = canvas.width - stickMargin - stickWidth;

        context.fillRect(posX, posY, 5, stickHeight);
    }

    // dibuja la pelota
    function drawBall() {
        context.fillStyle = "#fff";
        context.beginPath();
        context.arc(ball.x, ball.y, ballRadius, 0, Math.PI * 2, true);
        context.fill();
    }

    // estudio del mundo (paredes, entradas de usuario...)
    function input() {

        if (lostP1 == true || lostP2 == true)
            return;

        if (ball.x < 0) {
            pointsPlay2 += 1;
            lostP1 = true;
            return;
        }

        if (ball.x > canvas.width) {
            pointsPlay1 += 1;
            lostP2 = true;
            return;
        }

        // rebote de la pelota en las partes inferiores
        if (ball.y > canvas.height || ball.y < 0) {
            angle = 360 - angle;
            return;
        }

        // control rebote jugador 1
        if (ball.x < stickMargin + stickWidth && ball.y > posYP1 && ball.y < posYP1 + stickHeight) {
            angle = 180 - angle;
            return;
        }

        // control rebote jugador 2
        if (ball.x > canvas.width - stickMargin - stickWidth && ball.y > posYP2 && ball.y < posYP2 + stickHeight) {
            angle = 180 - angle;
            return;
        }
    }

    // Gestión de la pulsación de la tecla
    function keyDownHandler(e) {
        if (e.keyCode == 81) {
            P1isMovig = -1;
        } else if (e.keyCode == 65) {
            P1isMovig = 1;
        }
        if (e.keyCode == 80) {
            P2isMovig = -1;
        } else if (e.keyCode == 76) {
            P2isMovig = 1;
        }

        e.preventDefault();
    }

    // Gestión de la liberación de la tecla
    function keyUpHandler(e) {
        if (e.keyCode == 81 || e.keyCode == 65) {
            P1isMovig = 0;
        }

        if (e.keyCode == 80 || e.keyCode == 76) {
            P2isMovig = 0;
        }

        e.preventDefault();
    }

    // actualiza posiciones
    function update() {
        // nueva posicion pelota
        radians = angle * Math.PI / 180;
        ball.x += Math.cos(radians) * speedB;
        ball.y += Math.sin(radians) * speedB;

        // nueva posicion jugador 1
        if (P1isMovig != 0)
            posYP1 += P1isMovig * speedP;

        // nueva posicion jugador 2
        if (P2isMovig != 0)
            posYP2 += P2isMovig * speedP;
    }

    // bucle principal del juego
    // se compone de tres partes: input, update, draw
    function gameLoop() {

        window.setTimeout(gameLoop, loopInc); // cada loopInc ms

        // obtengo datos de la entrada de usurio
        input();

        // actualizo posiciones
        update();

        // redibujo la pantalla
        drawScreen();
    }


    initGame();
    gameLoop();

}