#README / LEEME

*Práctica:* 14

*Unidad:* UD06. Canvas HTML5

*Módulo:* Diseño de Interfaces WEB

*Ciclo Formativo:* Desarrollo de Aplicaciones WEB

*Objetivo:* Utilizar los conceptos básicos del dibujo del canvas y entender el concepto báscio de funcionamiento de un juego.

##Author / Autor

Alfredo Oltra (mail: [alfredo.ptcf@gmail.com](alfredo.ptcf@gmail.com) / twitter: [@aoltra](https://twitter.com/aoltra))

##License / Licencia

Creative Commons: [by-no-sa](http://es.creativecommons.org/blog/licencias/) 

